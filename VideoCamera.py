import threading

import face_recognition
from PIL import Image
import cv2
import os
import numpy
import csv
import telegram.bot
import time

from telegram import TelegramError
from telegram.error import Unauthorized, BadRequest, TimedOut, NetworkError, ChatMigrated


class VideoCamera(object):
    # Initialize some variables
    TOKEN = "520242096:AAHCNCp-xUHqdG4-PYmRSnF5hVxaC-OL1SQ"
    bot = telegram.Bot(token=TOKEN)
    message = "hemos detectado la presencia de un intruso. Le sugerimos que tome las medidas de seguridad respectivas."
    img = []
    result = [];
    rows_training = 0;
    face_locations = []
    face_encodings = []
    face_names = []
    process_this_frame = True
    photograms = 0
    tolencia = 30

    def __init__(self):
        self.video = cv2.VideoCapture(0)
        count = 1
        path = './db_faces'

        imagepath = [os.path.join(path, f) for f in os.listdir(path)]
        c = len(imagepath)
        # for i in imagepath: i access the each images from my folder of images
        while count <= c:
            image = face_recognition.load_image_file("./db_faces/" + str(count) + ".jpg")
            # now i will make list of the encoding parts to compare it runtime detected face
            self.img.append(face_recognition.face_encodings(image)[0])
            count = count + 1

        reader = csv.reader(open("faces-names.csv", "r"), delimiter=";")
        x = list(reader)
        self.result = numpy.array(x)
        self.rows_training = len(self.result)
        print('Entra al inicio')

    def __del__(self):
        self.video.release()

    def get_frame(self):
        # Grab a single frame of video
        ret, frame = self.video.read()

        # Resize frame of video to 1/4 size for faster face recognition processing
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

        # Only process every other frame of video to save time
        if self.process_this_frame:
            # Find all the faces and face encodings in the current frame of video
            face_locations = face_recognition.face_locations(small_frame)
            face_encodings = face_recognition.face_encodings(small_frame, face_locations)

            face_names = []
            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                match = face_recognition.compare_faces(self.img, face_encoding)
                name = "Desconocido"
                count = 0
                bandera = True
                while count <= self.rows_training - 1:
                    if match[count] == True:
                        name = str(self.result[count][1])
                        bandera = False
                        self.photograms = 0
                    count = count + 1
                face_names.append(name)

                if bandera:
                    self.photograms += 1
                    print(self.photograms)
                    if (self.photograms >= self.tolencia):
                        self.send_message_telegram()
                        self.photograms = 0
                        print(self.photograms)

        process_this_frame = not self.process_this_frame

        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()

    def send_message_telegram(self):
        updates = self.bot.get_updates()
        members_set = set()
        for u in updates:
            chat_id = u.message.chat.id
            user_id = u.message.chat.first_name
            members_set.add((chat_id, user_id))

        for s in members_set:
            id, user = s
            self.bot.send_message(chat_id=id, parse_mode='Markdown',
                                  text="Estimado/a: " + str(user) + " " + self.message)

# print([u.message.chat.id for u in updates])

# https://api.telegram.org/bot503871681:AAF6yKRZzIYhPj-9-_jRuiJzacZ6IfjDd6I/getUpdates
# curl -F "chat_id=460305061" -F "text=Es un texto de prueba" https://api.telegram.org/bot503871681:AAF6yKRZzIYhPj-9-_jRuiJzacZ6IfjDd6I/sendMessage
# curl -X POST "https://api.telegram.org/bot503871681:AAF6yKRZzIYhPj-9-_jRuiJzacZ6IfjDd6I/sendMessage" -d "chat_id=460305061&text=Bienvenido A SocialScan Alert, por favor para configurar la alerta siga las instrucciones de la web."
